# 切片
# 前两个元素
L[0:2]
L[:2]
# 倒数第二个和倒数第一个
L[-2:]
# 只有倒数第二个
L[-2:-1]
# 加入步长之后
L[:10:2]


# 迭代
d = {'a': 1, 'b': 2, 'c': 3}
# 默认只取到 key
for key in d:
	print key
# 这样来取 value
for value in d.itervalues():
	print value
# 这样来取 整个 key-value
for dic in d.iteritems():
	print dic

# 判断是否可迭代
from collections import Iterable
isinstance('abc', Iterable)
isinstance([1, 2, 3], Iterable)
isinstance(123, Iterable)

# 两个参数迭代
# 用 enumerate 把 list 变成和索引的键值对
for i, value in enumerate(['A', 'B', 'C']):
	print i, value
# 另外的情况
for x, y in [(1, 1), (2, 4), (3, 9)]:
    print x, y
# 三个也是可以的
for x, y,z in [(1, 1, 1), (2, 4, 8)]:
    print x, y, z


# 列表生成式，其实就是能方便的对一个列表在进行二次操作生成新的列表
[x * x for x in range(1, 11)]
# [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
# 还可以加判断
[x * x for x in range(1, 11) if x % 2 == 0]
# 嵌套循环
[m + n for m in 'ABC' for n in 'DEF']
# 用来转换大小写例子
L = ['Hello', 'World']
[s.lower() for s in L]
# 输出本地文件
import os
[d for d in os.listdir('.')]

# 再用上 isinstance 函数
L = ['Hello', 'World', 18]
[s.lower() if isinstance(s,str) else s for s in L]


# 生成器
g = (x * x for x in range(10))
g.next()
# 可以使用迭代
for n in g:
	print n

# 原函数
def fab(max):
    n, a, b = 0, 0, 1
    while n < max:
        print b
        a, b = b, a + b
        n = n + 1
# 返回 generator 对象的
def fab(max):
	n, a, b = 0, 0, 1
	while n < max:
		yield b
		a, b = b, a + b
		n = n + 1
# 调用 next() 会开始执行，执行到 yield 后暂且返回
def odd():
    print 'step 1'
    yield 1
    print 'step 2'
    yield 3
    print 'step 3'
    yield 5
