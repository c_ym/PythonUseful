#!/usr/bin/env python
# -*- coding: utf-8 -*-
# $ chmod a+x [filename].py 或者 $ chmod 755 [filename].py
# $ ./[filename].py

# 简单 print 输出字符串，使用 ',' 使得字符串连接中间会加入空格
print 'hello world!'
print 'The quick brown fox', 'jumps over', 'the lazy dog'
# 数字会自动当作字符串输出哦
print '100 + 200 =', 100 + 200

# 从命令行获取输入并赋值，可以不加提示
name = raw_input('input something: ');
print 'you say', name

# 可以用 '\' 来输出转移字符
print 'I\'m \"OK\"!'

# Python 交互式命令中输入多行字符串
>>> print '''line1
... line2
... line3'''
line1
line2
line3

# 逻辑值和它们的运算
True False
and not or

# 一个为空的特殊对象
None

# 字符替换
'Hi, %s, you have $%d.' % ('Michael', 1000000)

# 前面加 u 是 unicode 编码，命令设置用 gb2312 编码
u'中文'.encode('gb2312')

# list 可变表的用法
classmates = ['Michael', 'Bob', 'Tracy']
classmates[3]
classmates[-1]
classmates.append('Adam')
classmates.insert(1, 'Jack')
classmates.pop()
classmates.pop(1)
# list 中可以放各种各样的类型
L = ['Apple', 123, True]
s = ['python', 'java', ['asp', 'php'], 'scheme']
L = []

# tuple 不可变表
t = (1,)
t = ('a', 'b', ['A', 'B'])
classmates = ('Michael', 'Bob', 'Tracy')

# 判断句
age = 3
if age >= 18:
    print 'adult'
elif age >= 6:
    print 'teenager'
else:
    print 'kid'

# 遍历
names = ['Michael', 'Bob', 'Tracy']
for name in names:
    print names

# 获得填充了 0-100 数字的 list
range(101)

# 循环
sum = 0
n = 99
while n > 0:
	sum += n
	n -= 2
print sum

# 对输入的字符串转换类型
age = int(raw_input('请输入你的年龄'))

# dic 字典，key-value
d = {'Michael': 95, 'Bob': 75, 'Tracy': 85}
d['Michael'] = 79
# 检测有没有对应的 key
'Thomas' in d
d.get('Thomas')
d.get('Thomas', -1)
d.has_key('Thomas')
# 删除
d.pop('Bob')

# set 元素不重复
s = set([1, 1, 2, 3])
s.add(3)
s.remove(2)
s1 & s2
s1 | s2

# 会产生新的字符串
s = 'abc'
s = s.replace('a', 'b')