# 类和实例
class Student(object):
	pass

bart = Student()

# 实际例子
class Student(object):

    def __init__(self, name, score):
        self.name = name
        self.score = score

    def print_score(self):
        print '%s: %s' % (self.name, self.score)

>>> bart = Student('Bart Simpson', 59)
>>> bart.print_score()

# 允许为实例对象绑定任何变量
bart.age = 8
bart.xx = 'xx'


# 访问限制，设置私有变量
class Student(object):
    def __init__(self, name, score):
        self.__name = name
        self.__score = score
    def print_score(self):
        print '%s: %s' % (self.__name, self.__score)
# 外面不能够直接访问，但是可以再新创建这样的变量，当然最好不要这样做

# 可以设置 set get 函数
class Student(object):
    ...

    def get_name(self):
        return self.__name

    def get_score(self):
        return self.__score

    def set_score(self, score):
        if 0 <= score <= 100:
            self.__score = score
        else:
            raise ValueError('bad score')

# __xx__ 变量名是特殊变量，可以直接访问
# 刚才的 __name 是被改成了 _Student__name 这样还是可以访问


# 继承和多态
class Animal(object):
    def run(self):
        print 'Animal is running...'

class Dog(Animal):
    def run(self):
        print 'Dog is running...'

class Cat(Animal):
    def run(self):
        print 'Cat is running...'


d = Dog()
isinstance(d, Animal)

# 多态
def run_twice(animal):
    animal.run()
    animal.run()

run_twice(Animal())
run_twice(Dog())
run_twice(Cat())


# 获取对象信息
type(123)
type('sdf')
type(None)
type(abs)

import types
types.StringType
types.ListType

isinstance(d, Dog)

dir(int)

getattr()
setattr()
hasattr()

getattr(obj, 'x')
# 也可以加上得不到时候的默认值
getattr(obj, 'x', 123)

