# 函数调用
abs(-20)
cmp(1, 2)

# 函数帮助
help(abs)

# 数据类型转换
int('123')

# 函数对象
a = abs
a(-1)

# 定义自己的函数
def my_abs(x):
	if x >= 0:
		return x
	else:
		return -x

# 使用 pass 来占位
def nop():
	pass
# 占位是的程序能通过，之后再修改成实际代码
if a > b:
	pass

# 简单坚持输入数据的数据类型
def my_abs(x):
    if not isinstance(x, (int, float)):
        raise TypeError('bad operand type')
    if x >= 0:
        return x
    else:
        return -x

# 默认参数，必须放在后面
def power(x, n=2):
    s = 1
    while n > 0:
        n = n - 1
        s = s * x
    return s

# 使用 None 来解决使用 [] 时的问题
def add_end(L=None):
    if L is None:
        L = []
    L.append('END')
    return L

# 此时传入的数据需要是 list 或 tuple
# 如：calc([1, 2, 3])
def calc(numbers):
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum

# 此时使用可变参数，另外一层面的概念
# 会把参数自动组装成 tuple
# 如：calc(1, 3, 5, 7)
# 还可以组装后再调用：calc(*(1, 2, 3))
def calc(*numbers):
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum

# 关键字参数，内部自动组装成 dict
# person('Li', 30)
# person('Tom', 16, city = 'Beijing', job = 'Engineer')
# 同样可以先组装再调用：person('Tom', 16, **{'city = Beijing'})
def person(name, age, **kw):
    print 'name:', name, 'age:', age, 'other:', kw

# 参数组合，必须要按照顺序来
def func(a, b, c=0, *args, **kw):
    print 'a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw

# 递归函数
def fact(n):
    if n==1:
        return 1
    return n * fact(n - 1)