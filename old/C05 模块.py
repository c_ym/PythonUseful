# 下面的内容是一整个自定义的模块
# -----------------------
#!/usr/bin/env python
# -*- coding: utf-8 -*-

'Just a test module.'

__author__ = 'BinderClip'

import sys

def test():
	args = sys.argv
	hello = 'Hello'
	if len(args) == 1:
		hello += ', '
		hello += 'world'
	else:
		for x in args[1:]:
			hello += ', '
			hello += x
	print hello

if __name__=='__main__':
	test() 
# -----------------------

# 使用别名
try:
    import cStringIO as StringIO
except ImportError: # 导入失败会捕获到ImportError
    import StringIO


# 习惯会把内部的变量、函数使用 '_' 开头来表示，但是不能够阻止调用
def _private_1(name):
    return 'Hello, %s' % name

def _private_2(name):
    return 'Hi, %s' % name

def greeting(name):
    if len(name) > 3:
        return _private_1(name)
    else:
        return _private_2(name)


# 安装模块
easy_install PIL
# 使用它里面的
import Image
im = Image.open('test.png')
print im.format, im.size, im.mode
im.thumbnail((140, 90))
im.save('thumb.png', 'PNG')

# 添加模块需要在已经添加的 Path 下，可以用下面的查看：
import sys
sys.path
# 添加自己的搜索目录
# 1. 临时，运行时有效
# sys.path.append('[path to add]')
# 2. 全局，所有都有效
# 为系统设置 PYTHONPATH 变量


# 使用 __future__
# 就是为了使用到新版本里面的一些特性，由此来做测试
# 使用了 3.x 里面的有关字符串的特性
from __future__ import unicode_literals
print '\'xxx\' is unicode?', isinstance('xxx', unicode)
print 'u\'xxx\' is unicode?', isinstance(u'xxx', unicode)
print '\'xxx\' is str?', isinstance('xxx', str)
print 'b\'xxx\' is str?', isinstance(b'xxx', str)
# $ python task.py
# 'xxx' is unicode? True
# u'xxx' is unicode? True
# 'xxx' is str? False
# b'xxx' is str? True

# 2.x 中的地板除和精确除
10 / 3
10.0 / 3
# 3.x 中的地板除和精确除
10 // 3
10 / 3
# 2.x 中使用 3.x 的除法
from __future__ import division