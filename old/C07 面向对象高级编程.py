# 绑定属性和使用 __slots__ 限制绑定
class Student(object):
	pass
# 动态绑定属性
s = Student()
s.name = 'Michael'
# 动态绑定方法
def set_age(self, age):
	self.age = age
# 给某个实例对象绑定方法
from types import MethodType
s.set_age = MethodType(set_age, s, Student)
s.set_age(25)
# 给整个类绑定方法
Student.set_age = MethodType(set_age, None, Student)

# 可以使用 __slots__ 加以限制
class Student(object):
	__slots__ = ('name', 'age') # 用tuple定义允许绑定的属性名称
# 这时候即使是在 __init__ 中也不可以哦
# 子类中再使用 __slots__ 可以扩展可用的范围


# 使用 @property
# 为了暴露属性+检查参数，可以使用 getter 和 setter
class Student(object):
	def get_score(self):
		return self._score

	def set_score(self, value):
		if not isinstance(value, int):
			raise ValueError('score must be an integer!')
		if value < 0 or value > 100:
			raise ValueError('score must between 0 ~ 100!')
		self._score = value

# 对应这种情况，可以使用 @property 来进行简化
class Student(object):

    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        if not isinstance(value, int):
            raise ValueError('score must be an integer!')
        if value < 0 or value > 100:
            raise ValueError('score must between 0 ~ 100!')
        self._score = value

# 对应的只设置 getter 而不设置 setter 就产生了只读属性
class Student(object):

    @property
    def birth(self):
        return self._birth

    @birth.setter
    def birth(self, value):
        self._birth = value

    @property
    def age(self):
        return 2014 - self._birth


# 多重继承 Mixin，与只允许单继承的 Java 不同
class Dog(Mammal, RunnableMixin, CarnivorousMixin):
    pass


# 定制类
# 前面说过的 __slots__ __len__
# __str__ 类似 Java 里面的 toString
# __str__ 可以自定 print 的时候显示的内容
# __repr__ 用于显示直接打入实例名字的时候显示的调试内容
class Student(object):
	def __init__(self, name):
		self.name = name
	def __str__(self):
		return 'Student object (name: %s)' % self.name
	__repr__ = __str__

# __iter__ 为了支持迭代，需要支持这个函数来反悔一个可迭代对象
# 之后会调用 next() 函数来获得下一个对象
class Fib(object):
    def __init__(self):
        self.a, self.b = 0, 1 # 初始化两个计数器a，b

    def __iter__(self):
        return self # 实例本身就是迭代对象，故返回自己

    def next(self):
        self.a, self.b = self.b, self.a + self.b # 计算下一个值
        if self.a > 100000: # 退出循环的条件
            raise StopIteration();
        return self.a # 返回下一个值
# __getitem__ 为了可以使用 a[6] 这样的获取方法
class Fib(object):
    def __getitem__(self, n):
        a, b = 1, 1
        for x in range(n):
            a, b = b, a + b
        return a
# 还有支持了切片以后
class Fib(object):
    def __getitem__(self, n):
        if isinstance(n, int):
            a, b = 1, 1
            for x in range(n):
                a, b = b, a + b
            return a
        if isinstance(n, slice):
            start = n.start
            stop = n.stop
            a, b = 1, 1
            L = []
            for x in range(stop + 1):
                if x >= start:
                    L.append(a)
                a, b = b, a + b
            return L
# 如果对象是 dict __getitem__() 还需要作为 key 的 Object，还有 __setitem__()，最后还有 __delitem__()

# __getattr__ 如果参数找不到的时候就会调用这个函数，可以自己来修饰
class Student(object):

    def __init__(self):
        self.name = 'Michael'

    def __getattr__(self, attr):
        if attr=='score':
            return 99
# 这样本来还没有属性 score 的时候去调用都可以返回 99

# 只处理部分没有的属性
class Student(object):

    def __getattr__(self, attr):
        if attr=='age':
            return lambda: 25
        raise AttributeError('\'Student\' object has no attribute \'%s\'' % attr)

# 链式调用
class Chain(object):

    def __init__(self, path=''):
        self._path = path

    def __getattr__(self, path):
        return Chain('%s/%s' % (self._path, path))

    def __str__(self):
        return self._path

# __call__ 直接输入实例名字加括号时 a() 调用的函数
lass Student(object):
    def __init__(self, name):
        self.name = name

    def __call__(self):
        print('My name is %s.' % self.name)

# 使用 callable() 函数判断某个对象或者实例是不是可以调用的


# 使用元类
# 先使用 type() 来动态创建类
def fn(self, name='world'): # 先定义函数
    print('Hello, %s.' % name)

Hello = type('Hello', (object,), dict(hello=fn)) # 创建Hello class

# metaclass
# metaclass是创建类，所以必须从`type`类型派生：
# 下面程序是把要创建的类的属性名称都变为大写
class ListMetaclass(type):
    def __new__(cls, name, bases, attrs):
        attrs['add'] = lambda self, value: self.append(value)
        return type.__new__(cls, name, bases, attrs)

class MyList(list):
    __metaclass__ = ListMetaclass # 指示使用ListMetaclass来定制类

# __new__()方法接收到的参数依次是：
# 1. 当前准备创建的类的对象；
# 2. 类的名字；
# 3. 类继承的父类集合；
# 4. 类的方法集合。

# 编写 ORM 框架
class User(Model):
    # 定义类的属性到列的映射：
    id = IntegerField('id')
    name = StringField('username')
    email = StringField('email')
    password = StringField('password')

# 创建一个实例：
u = User(id=12345, name='Michael', email='test@orm.org', password='my-pwd')
# 保存到数据库：
u.save()

class Field(object):
    def __init__(self, name, column_type):
        self.name = name
        self.column_type = column_type
    def __str__(self):
        return '<%s:%s>' % (self.__class__.__name__, self.name)

class StringField(Field):
    def __init__(self, name):
        super(StringField, self).__init__(name, 'varchar(100)')

class IntegerField(Field):
    def __init__(self, name):
        super(IntegerField, self).__init__(name, 'bigint')

class ModelMetaclass(type):
    def __new__(cls, name, bases, attrs):
        if name=='Model':
            return type.__new__(cls, name, bases, attrs)
        mappings = dict()
        for k, v in attrs.iteritems():
            if isinstance(v, Field):
                print('Found mapping: %s==>%s' % (k, v))
                mappings[k] = v
        for k in mappings.iterkeys():
            attrs.pop(k)
        attrs['__table__'] = name # 假设表名和类名一致
        attrs['__mappings__'] = mappings # 保存属性和列的映射关系
        return type.__new__(cls, name, bases, attrs)

# 基类
class Model(dict):
    __metaclass__ = ModelMetaclass

    def __init__(self, **kw):
        super(Model, self).__init__(**kw)

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(r"'Model' object has no attribute '%s'" % key)

    def __setattr__(self, key, value):
        self[key] = value

    def save(self):
        fields = []
        params = []
        args = []
        for k, v in self.__mappings__.iteritems():
            fields.append(v.name)
            params.append('?')
            args.append(getattr(self, k, None))
        sql = 'insert into %s (%s) values (%s)' % (self.__table__, ','.join(fields), ','.join(params))
        print('SQL: %s' % sql)
        print('ARGS: %s' % str(args))

u = User(id=12345, name='Michael', email='test@orm.org', password='my-pwd')
u.save()


