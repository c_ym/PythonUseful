#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 打印脚本文件所在路径
# 似乎并非很容易
# 相关的问题 [How do I get the path of the current executed file in python? - Stack Overflow](http://stackoverflow.com/questions/2632199/how-do-i-get-the-path-of-the-current-executed-file-in-python)
# 但是这里主要考虑这样的情况：写一个脚本文件，直接双击打开，cwd 的值是当前用户的根目录

import os, sys

def main():
	print('cwd: ' + os.getcwd())
	path = os.path.abspath(os.path.dirname(sys.argv[0]))
	print('swd: ' + path)
	# print(sys.argv)
	# print(os.path.dirname(sys.argv[0]))
	# print(os.path.abspath(os.path.dirname(sys.argv[0])))
	input('Enter to end...')

if __name__ == '__main__':
	main()