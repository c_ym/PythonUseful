#!/usr/bin/env python
# -*- coding: utf-8 -*-

# 打印当前工作路径，可具体的 Python 脚本存放的位置无关

import os

def main():
	print(os.getcwd())

if __name__ == '__main__':
	main()