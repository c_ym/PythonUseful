# PythonUseful 【重新构建中...】

一些 Python 中常用基础内容的收集。主要以实用的小代码片段为主。会尽量使用 Python 3。

## 外部资源索引

## 内部代码索引

### 模板 template

- 基础 [basic](./template/basic)
	- 基础模板文件 [template.py](./template/basic/template.py)
	- 带有 main 函数 [template_main.py](./template/basic/template_main.py) 
- 标准输入输出
- 文件系统 [filesystem](./template/filesystem)
	- 获取当前路径 [cwd.py](./template/filesystem/cwd.py)
	- 获取脚本所在的路径（主要用于在 Finder 中直接打开）[swd](./template/filesystem/swd) [swd.py](./template/filesystem/swd.py)
	- 打印当前路径内容
	- 遍历子目录的内容


## TODO

- 等到文件足够多之后可以考虑写自动生成索引的脚本，提取类似文档注释的内容

## 最后

慢慢来，遇到要什么东西就简单整理一下有用的东西出来，这样将来就可以随手拿来用了，不把时间浪费在重复的小事情上。加油！